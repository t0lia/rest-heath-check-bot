# build backend
FROM maven:3.6.1-amazoncorretto-11 AS MAVEN_TOOL_CHAIN

COPY backend/ /tmp/

WORKDIR /tmp/

RUN mvn clean package

# production
FROM amazoncorretto:11

RUN mkdir /app
COPY --from=MAVEN_TOOL_CHAIN /tmp/target/*.jar /app/app.jar

ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom","-jar","/app/app.jar"]
