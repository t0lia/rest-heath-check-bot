package com.apozdniakov.resthealthcheckbot;

public interface Notifier {
    void notify(String textMsg);
}
