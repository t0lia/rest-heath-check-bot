package com.apozdniakov.resthealthcheckbot;

import io.sentry.Sentry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

@SpringBootApplication
public class HealthCheckApplication implements CommandLineRunner {

    private static Logger LOG = LoggerFactory
            .getLogger(HealthCheckApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(HealthCheckApplication.class, args);
    }

    private RestHealthCheckerFactory factory;
    private Notifier notifier;
    private int duration;
    private String sentryDsn;

    @Autowired
    public HealthCheckApplication(RestHealthCheckerFactory factory,
                                  Notifier notifier,
                                  @Value("${healthcheck.duration}") int duration,
    @Value("${healthcheck.sentry.dsn}") String sentryDsn) {
        this.factory = factory;
        this.notifier = notifier;
        this.duration = duration;
        this.sentryDsn = sentryDsn;
    }

    @Override
    public void run(String... args) {
        Sentry.init(options -> options.setDsn(sentryDsn));

        try {
            exec();
        } catch (Exception e) {
            Sentry.captureException(e);
        }
    }

    private void exec() throws InterruptedException {
        List<HealthChecker> healthCheckers = factory.get();

        while (true) {
            LOG.info("START EXECUTING REQUESTS");
            healthCheckers.forEach(healthChecker -> {
                LOG.info("check for {}", healthChecker.getName());
                HealtStatus status = healthChecker.check();
                if (!status.isAlive()) {
                    notifier.notify("ALERT:" + healthChecker.getName() + " is down");
                }
                if (status.getCode() != 200) {
                    notifier.notify("ALERT:" + healthChecker.getName() + " code:  " + status.getCode() + " info: " + status.getInfo());
                }
            });
            Thread.sleep(duration * 1000);
        }
    }
}
