package com.apozdniakov.resthealthcheckbot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;


public class SimpleRestHealthChecker implements HealthChecker {
    private final String target;

    private static Logger LOG = LoggerFactory.getLogger(SimpleRestHealthChecker.class);

    public SimpleRestHealthChecker(String target) {
        this.target = target;
    }

    public HealtStatus check() {

        try {
            LOG.info(target);
            HttpClient client = HttpClient.newHttpClient();
            HttpRequest request = HttpRequest.newBuilder()
                    .uri(URI.create(target + "/actuator/health"))
                    .build();

            HttpResponse<String> response =
                    client.send(request, HttpResponse.BodyHandlers.ofString());

            LOG.info("response code = {}", response.statusCode());
            HealtStatus healtStatus = new HealtStatus();
            healtStatus.setAlive(true);
            healtStatus.setCode(response.statusCode());
            healtStatus.setInfo(response.body());
            return healtStatus;
        } catch (InterruptedException | IOException e) {
            e.printStackTrace();
        }

        HealtStatus healtStatus = new HealtStatus();
        healtStatus.setAlive(false);
        return healtStatus;
    }

    @Override
    public String getName() {
        return target;
    }

}
