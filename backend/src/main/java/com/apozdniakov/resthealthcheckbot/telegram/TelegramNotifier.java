package com.apozdniakov.resthealthcheckbot.telegram;

import com.apozdniakov.resthealthcheckbot.Notifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.List;

/**
 * Notification over telegram.
 */
@Service
class TelegramNotifier implements Notifier {

    private static Logger LOG = LoggerFactory.getLogger(TelegramNotifier.class);

    private final List<String> subscribers;
    private final TelegramLongPollingBot bot;

    /**
     * Base constructor.
     *
     * @param subscribers list of telegram subscribers id
     * @param bot         bot
     */
    TelegramNotifier(@Value("${healthcheck.bot.subscribers}") String[] subscribers,
                     TelegramLongPollingBot bot) {
        this.subscribers = Arrays.asList(subscribers);
        this.bot = bot;
    }

    /**
     * Telegram bot api initialize.
     */
    @PostConstruct
    protected void init() {
        if (bot.getBotToken().isEmpty()) {
            LOG.warn("Start without telegram notifier");
            return;
        }

        ApiContextInitializer.init();
        TelegramBotsApi telegramBotsApi = new TelegramBotsApi();
        try {
            telegramBotsApi.registerBot(bot);
        } catch (TelegramApiException e) {
            LOG.error("error bot setup", e);
        }
    }

    /**
     * Notify subscribers.
     *
     * @param textMsg message to sent.
     */
    public void notify(String textMsg) {
        subscribers.stream()
                .map(sub -> new SendMessage()
                        .setChatId(sub)
                        .setText(textMsg)
                )
                .forEach(msg -> {
                    try {
                        bot.execute(msg);
                    } catch (TelegramApiException e) {
                        LOG.error("Failed to send notification via telegram to user {}", msg.getChatId(), e);
                    }
                });

    }

}
