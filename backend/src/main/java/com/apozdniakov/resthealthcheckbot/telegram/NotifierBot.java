package com.apozdniakov.resthealthcheckbot.telegram;

import org.telegram.telegrambots.bots.DefaultBotOptions;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.objects.Update;


/**
 * Telegram notification bot.
 */
class NotifierBot extends TelegramLongPollingBot {

    private String token;
    private String name;


    /**
     * Base constructor.
     *
     * @param options bot options
     * @param token   bot token
     * @param name    bot name
     */
    NotifierBot(DefaultBotOptions options, String token, String name) {
        super(options);
        this.token = token;
        this.name = name;
    }

    /**
     * OnUpdateReceived handler.
     *
     * @param update update
     */
    @Override
    public void onUpdateReceived(Update update) {
        // nothing here
    }

    /**
     * Bot username.
     *
     * @return bot username
     */
    @Override
    public String getBotUsername() {
        return name;
    }

    /**
     * Bot token.
     *
     * @return bot token
     */
    @Override
    public String getBotToken() {
        return token;
    }
}
