package com.apozdniakov.resthealthcheckbot.telegram;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.telegram.telegrambots.bots.DefaultBotOptions;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;

/**
 * Configuration for telegram notifier.
 */
@Configuration
public class TelegramConfiguration {

    /**
     * Telegram bot implementation.
     *
     * @param token bot token
     * @param name  bot name
     * @return bot implementation
     */
    @Bean
    public TelegramLongPollingBot telegramLongPollingBot(
            @Value("${healthcheck.bot.token}") String token,
            @Value("${healthcheck.bot.name}") String name) {
        return new NotifierBot(new DefaultBotOptions(), token, name);
    }
}
