package com.apozdniakov.resthealthcheckbot;

public interface HealthChecker {
    HealtStatus check();

    String getName();
}
