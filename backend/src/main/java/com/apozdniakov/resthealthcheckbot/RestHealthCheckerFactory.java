package com.apozdniakov.resthealthcheckbot;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.function.Supplier;

import static java.util.stream.Collectors.toList;


@Component
public class RestHealthCheckerFactory implements Supplier<List<HealthChecker>> {
    @Value("${healthcheck.targets}")
    List<String> targets;

    @Override
    public List<HealthChecker> get() {
        return targets.stream().map(SimpleRestHealthChecker::new).collect(toList());
    }

}
